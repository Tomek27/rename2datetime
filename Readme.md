# rename2datetime
Renames all files in a given `path` 
if it contains the exif date time original tag

from to the old file name into

\<year\>\<month\>\<day\>\_\<hour\>\<minutes\>\<seconds\>[.\<file extension\>]

## Usage
```
rename2datetime /path/to/files
```
