extern crate rexif;

use std::env;
use std::fs;
use std::path::{Path, PathBuf};
use std::process::exit;

use rexif::{ExifData, ExifTag};

const HELP: &str = "
usage: rename-pic-timestamp <folder>
	<folder> folder to rename pics in";

fn main() {
    let path = get_path();
    println!("Renaming in: {:?}", &path);
    renaming(&path);
}

fn renaming(path: &Path) {
    for file in path.read_dir().expect("reading dir") {
        let file = file.expect("unwrap file");
        let file = file.path();
        let filename = file.file_name();
        let filename = filename.expect("file name");
        let filename = filename.to_string_lossy();

        match rexif::parse_file(&file) {
            // get the Exif Data
            Ok(exif) => {
                // get the parsed timestamp
                if let Some(timestamp) = get_timestamp(&exif) {
                    let extension = file.extension();
                    let extension = extension.expect("file extension");
                    let extension = extension.to_string_lossy();
                    let new_filename = format!("{}.{}", &timestamp, &extension);
                    let mut new_file = PathBuf::new();
                    new_file.push(&path);
                    new_file.push(&new_filename);

                    match fs::rename(&file, &new_file) {
                        Ok(()) => println!("renaming: {} -> {}", &filename, &new_filename),
                        Err(e) => println!("failed renaming of {}: {:?}", &filename, &e),
                    }
                } else {
                    println!("skipping: {}", &filename);
                }
            },
            Err(e) => println!("failed parsing of {}: {:?}", &filename, &e),
        }
    }
}

fn get_timestamp(exif: &ExifData) -> Option<String> {
        for entry in &exif.entries {
            if entry.tag == ExifTag::DateTimeOriginal {
                return Some(format_timestamp(&entry.value_more_readable));
            }
        }
        None
}

/// Formats the Exif Date Time Stamp e.g. 2017:11:27 23:45:12
/// by removing the delimeter ':'
/// and adding an underscore between date and time
/// into 20171127_234512
fn format_timestamp(timestamp: &String) -> String {
    let timestamp = timestamp.replace(":", "");
    let timestamp = timestamp.replace(" ", "_");
    timestamp
}

fn get_path() -> Box<Path> {
	let path = env::args().nth(1);
	if path.is_none() {
		help();
	}

	let path = path.unwrap();
        let path = fs::canonicalize(&path);
	let mut pathbuf = PathBuf::new();

        match path {
            Ok(path) => pathbuf.push(path),
            Err(e) => {
                println!("Path is wrong: {:?}", &e);
                help();
            }
        }

        pathbuf.into_boxed_path()
}

fn help() {
	println!("{}", &HELP);
	exit(-1);
}
